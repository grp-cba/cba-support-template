### Project owner
  * EMBL unit
	- [ ] Developmental biology
	- [ ] Cell biology and biophysics
	- [ ] Molecular systems biology
	- [ ] Genome biology
	- [ ] Directors' research
	- [ ] Structural biology - Grenoble
	- [ ] Structural biology - Hamburg
	- [ ] Epigenetics and Neurobiology (Rome)
	- [ ] Tissue Biology and Disease Modelling (Barcelona)
	- [ ] Bioinformatics (EBI research)
	- [ ] Support services - Heidelberg
	- [ ] Support services - Rome
	- [ ] Support services - Barcelona
	- [ ] Support services - Hamburg
	- [ ] Support services - Grenoble

### Image data producer
  - [ ] ALMF (Heidelberg)
  - [ ] EMCF (Heidelberg)
  - [ ] IC LM (Heidelberg)
  - [ ] IC EM (Heidelberg)
  - [ ] MIF (Barcelona)
  - [ ] LIF (Rome)
  - [ ] X-ray (Hamburg)
  - [ ] Lab microscope
  - [ ] Other:

### Sample
  * Type
	- [ ] Cell culture 2D
	- [ ] Cell culture 3D
	- [ ] Organoid
	- [ ] Tissue 2D
	- [ ] Tissue 3D
	- [ ] Whole organism
	- [ ] Single cell
	- [ ] Environmental sample
	- [ ] Other: 

  * High level taxonomic group
	- [ ] Bacteria
	- [ ] Archae
	- [ ] Protists
	- [ ] Animals
	- [ ] Fungi
	- [ ] Plants
	- [ ] Other: 

### Assay
  * [Imaging method](http://purl.obolibrary.org/obo/FBbi_00000222)
	- [ ] Cryo-EM
	- [ ] EM (non-cyro)
	- [ ] Super-resolution fluorescence microscopy 
	- [ ] Fluorescence microscopy (non super-resolution)
	- [ ] Other light-based imaging
	- [ ] Atomic force microscopy
	- [ ] X-ray imaging
	- [ ] Other: 
	
  * Measurement length scale
	- [ ] <1 nm
	- [ ] 1-10 nm
	- [ ] 10-100 nm
	- [ ] 100-1000 nm
	- [ ] 1-10 $\mu$m
	- [ ] 10-100 $\mu$m
	- [ ] 100-1000 $\mu$m
	- [ ] >1000 $\mu$m

### Data
  * Dimensions
	- [ ] x,y
	- [ ] z
	- [ ] t
	- [ ] c
  * [Input image format](https://purl.bioontology.org/ontology/EDAM?conceptid=http%3A%2F%2Fedamontology.org%2Fformat_1915)
    - [ ] [TIFF](https://purl.bioontology.org/ontology/EDAM?conceptid=http%3A%2F%2Fedamontology.org%2Fformat_3591)
	- [ ] [OME-TIFF](https://purl.bioontology.org/ontology/EDAM?conceptid=http%3A%2F%2Fedamontology.org%2Fformat_3590)
	- [ ] [HDF5](https://purl.bioontology.org/ontology/EDAM?conceptid=http%3A%2F%2Fedamontology.org%2Fformat_3590)
	- [ ] [Zarr](https://purl.bioontology.org/ontology/EDAM?conceptid=http%3A%2F%2Fedamontology.org%2Fformat_3915) (including OME-Zarr)
	- [ ] [AVI](https://purl.bioontology.org/ontology/EDAM?conceptid=http%3A%2F%2Fedamontology.org%2Fformat_3990)
	- [ ] [MPEG-4](https://purl.bioontology.org/ontology/EDAM?conceptid=http%3A%2F%2Fedamontology.org%2Fformat_3997)
	- [ ] [DSV](https://purl.bioontology.org/ontology/EDAM?conceptid=http%3A%2F%2Fedamontology.org%2Fformat_3751) (Delimiter-Separated Values tabular format)
	- [ ] Proprietary format
  * Total project size
	- [ ] <1 GB
	- [ ] 1-100 GB
	- [ ] 100-1000 GB
	- [ ] 1-10 TB
	- [ ] >10 TB
  * Image data storage
	- [ ] NFS volume (e.g. group shares)
	- [ ] S3 bucket
	- [ ] File sharing service (e.g. ownCloud, Google Drive ...)
	- [ ] Other:

### Bioimage data science tasks
  * Data handling
	- [ ] [Conversion](https://purl.bioontology.org/ontology/EDAM-BIOIMAGING?conceptid=http%3A%2F%2Fedamontology.org%2Foperation_3434)
	- [ ] Other: 
  * Image processing
	- [ ] [Drift correction](https://purl.bioontology.org/ontology/EDAM-BIOIMAGING?conceptid=http%3A%2F%2Fedamontology.org%2Foperation__RBh3vNecu5XlIPh3kbMM4Cq)
	- [ ] [Image registration](https://purl.bioontology.org/ontology/EDAM-BIOIMAGING?conceptid=http%3A%2F%2Fedamontology.org%2Foperation_Image_registration)
	- [ ] [Image enhancement](https://purl.bioontology.org/ontology/EDAM-BIOIMAGING?conceptid=http%3A%2F%2Fedamontology.org%2Foperation_Image_enhancement) (e.g. denoising, smoothing ...)
	- [ ] [Image reconstruction](https://purl.bioontology.org/ontology/EDAM-BIOIMAGING?conceptid=http%3A%2F%2Fedamontology.org%2Foperation_Image_reconstruction) (e.g. deconvolution, stitching, tomographic reconstruction ...)
	- [ ] [Image segmentation](https://purl.bioontology.org/ontology/EDAM-BIOIMAGING?conceptid=http%3A%2F%2Fedamontology.org%2Foperation_Image_segmentation)
	- [ ] Other: 
  * Analysis
    - [ ] [Object feature extraction](https://purl.bioontology.org/ontology/EDAM-BIOIMAGING?conceptid=http%3A%2F%2Fedamontology.org%2Foperation_Feature_extraction)
	- [ ] [Object tracking](https://purl.bioontology.org/ontology/EDAM-BIOIMAGING?conceptid=http%3A%2F%2Fedamontology.org%2Foperation_Image_tracking)
	- [ ] [Object counting](https://purl.bioontology.org/ontology/EDAM-BIOIMAGING?conceptid=http%3A%2F%2Fedamontology.org%2Foperation_Cell_counting)
	- [ ] [Classification](https://purl.bioontology.org/ontology/EDAM-BIOIMAGING?conceptid=http%3A%2F%2Fedamontology.org%2Foperation_2990) (i.e. image, pixel or object classification)
	- [ ] [Clustering](https://purl.bioontology.org/ontology/EDAM-BIOIMAGING?conceptid=http%3A%2F%2Fedamontology.org%2Foperation___Clustering)
	- [ ] [Single molecule localisation](https://purl.bioontology.org/ontology/EDAM-BIOIMAGING?conceptid=http%3A%2F%2Fedamontology.org%2Foperation__R8EfungVXA0oQM2Ac7YY5eu)
	- [ ] [Statistical calculation](http://edamontology.org/operation_2238) (i.e. statistical analysis)
	- [ ] [Visualisation](https://purl.bioontology.org/ontology/EDAM-BIOIMAGING?conceptid=http%3A%2F%2Fedamontology.org%2Foperation_0337) (e.g. surface/volume rendering, montage, overlay, plotting ...)
	- [ ] [Annotation](https://purl.bioontology.org/ontology/EDAM-BIOIMAGING?conceptid=http%3A%2F%2Fedamontology.org%2Foperation_022)
	- [ ] Other: 

### Software
  * Scripts
	- [ ] Python
	- [ ] Java
	- [ ] R
	- [ ] ImageJ macro
	- [ ] ImageJ jython
	- [ ] ImageJ groovy
	- [ ] Matlab
	- [ ] Nextflow
	- [ ] Other:
  * Tools
	- [ ] Fiji/ImageJ
	- [ ] Cellprofiler
	- [ ] Ilastik
	- [ ] Napari
	- [ ] Cellpose
	- [ ] StarDist
	- [ ] YOLO
	- [ ] YoloLabel
	- [ ] Anylabeling
	- [ ] Galaxy
	- [ ] MoBIE
	- [ ] Image Data Explorer
	- [ ] Imaris
	- [ ] Huygens
	- [ ] Other:

### IT infrastructure
  - [ ] [EMBL 3D Cloud](https://www.embl.org/about/info/it-services/3d-cloud/)
  - [ ] [BARD](https://bard.embl.de/)
  - [ ] EMBL HPC
  - [ ] Lab or facility workstation
  - [ ] Personal computer
  - [ ] Other: 	

### Keywords
  - [ ] Spatial omics
  - [ ] Other: 
