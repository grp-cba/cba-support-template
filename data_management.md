# Data managment

[[_TOC_]]

Description of the management of both the microscopy **input** and analysis **output** data.

## Imaging data adequacy

*Describe how the data is adequate with regards to the scientific question; also describe challenging aspects of the data.*

- Spatial sampling and resolution ...
- Temporal sampling and resolution ...
- Signal within specimen (e.g. signal loss when imaging deeper) ...
- ...

## Preprocessing

*Describe any preprocessing that has been done to the data before it was shared with the bioimage analysis service team (stitching, deconvolution, denoising, ...)*

## Data access

The data in the below location(s) has been shared [using the DMA](https://wiki.embl.de/itspublic/Data_Management_App)

- `/g/group/member/data`

## Naming schemes and file formats

*Describe the file and folder naming schemes, modifying the below template*

- `${DATE}_${CONDITION}_${REPLICATE_ID}.tif`, with
    - `DATE`: `YYYYMMDD`
- Example: `20230101_siKi67_1.tif`
- File-format: ImageJ-TIFF
- Channel information: e.g. ch1 (dapi), ch2 ....

## Minimal example / test data

Minimal example data that can be used to run and test the analysis workflow. 

In addition to image data, an actual validation of the results also requires text files which contain the expected analysis results in a machine-readable manner.

- *minimal example image data* 
- *corresponding expected analysis results as machine-readbale text files* 


## References

- [Characters to use and avoid in file and folder names](https://youtu.be/dLziBlI2qlo)
- [General data management considerations](https://git.embl.de/grp-cba/cba#data-management)


