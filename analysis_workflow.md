# Analysis workflow

[[_TOC_]]

Description of the analysis workflow, including instructions for visual QC of the analysis results.

## Measurements

The following analysis workflow is designed to extract the following information.

- *Number of...*
    - *The biological relevance is...*
- *Sum intensity of...*
    - *This reflects the expression level of...*

## Manual measurements

*...using Fiji ROIs or ilastik label few regions and measure...*

*...link to the manually annotated data and semi-automatically measured results...*

## Automated batch analysis

- Log into EMBL cluster: `ssh ${USERNAME}@login01.cluster.embl.de`
    - [Detailed cluster login instructions](https://git.embl.de/grp-cba/cba/-/blob/master/resources_access/using-embl-cluster.md?ref_type=heads#using-the-embl-hpc)
- **Only once** fetch the code repository: `git clone git@git.embl.de:grp-cba/${REPO}.git`
    - [Detailed GitLab instructions](https://git.embl.de/grp-cba/cba/-/blob/master/resources_access/using-embl-gitlab-repos.md?ref_type=heads#using-embl-gitlab-repositories)
- **Every time** update the code: `cd ${REPO}; git pull; cd ~`
- `module load Nextflow` 
- `source /g/cba/miniconda3/init_conda.sh`
- `nextflow ${REPO}/code/nextflow/${SCRIPT}.nf --inputDir ${IN_DIR} --outputDir ${OUT_DIR} -c ${REPO}/code/nextflow/nextflow.config -profile slurm`
- [Concatenating tables](https://git.embl.de/heriche/image-data-explorer/-/wikis/Preparing-the-data-for-use-with-the-IDE)

## Analysis steps

### Step 1

Input:
- ...

*Description of how to run the analysis*

Output:
- ...

### Step 2

Input:
- ...

*Description of how to run the analysis*

Output:
- ...


## Visual QC and exploration

- [Using MoBIE](https://git.embl.de/grp-cba/cba/-/blob/master/resources_access/using-mobie.md)
- [Using the Image Data Explorer](https://git.embl.de/heriche/image-data-explorer)

## Statistical data analysis

- For statistical analysis support at EMBL, consider contacting [CSDA](https://bio-it.embl.de/centres/csda/)

## References

- [Minimal overview about using Nextflow for bioimage analysis](https://docs.google.com/presentation/d/1QD-0t6_Us5bVsH_e1CWOoS4nw6tNK41IG2O2CmzYyhI/edit?usp=sharing)
- [The Image Data Explorer on GitLab](https://git.embl.de/heriche/image-data-explorer#image-data-explorer)
- [Muller C, Serrano-Solano B, Sun Y, Tischer C, Hériché JK. The Image Data Explorer: Interactive exploration of image-derived data. PLoS One. 2022 Sep 15;17(9):e0273698. doi: 10.1371/journal.pone.0273698. PMID: 36107835; PMCID: PMC9477257.](https://pubmed.ncbi.nlm.nih.gov/36107835/)
- [Di Tommaso, P., Chatzou, M., Floden, E. et al. Nextflow enables reproducible computational workflows. Nat Biotechnol 35, 316–319 (2017). https://doi.org/10.1038/nbt.3820](https://www.nature.com/articles/nbt.3820)
- [Pape, C., Meechan, K., Moreva, E. et al. MoBIE: a Fiji plugin for sharing and exploration of multi-modal cloud-hosted big image data. Nat Methods 20, 475–476 (2023). https://doi.org/10.1038/s41592-023-01776-4](https://www.nature.com/articles/s41592-023-01776-4)
