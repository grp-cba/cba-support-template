#!/usr/bin/env nextflow

segmentPythonScript = "${workflow.projectDir}/../python/segment.py"

process SEGMENT {
    publishDir "params.outputDir", mode: 'copy', overwrite: true

    input:
    tuple val(sampleID), path(inputImagePath)

    output:
    tuple val(sampleID), path("*${params.segmentSuffix}.tif")

    debug true

    // https://github.com/BioImageTools/ome-zarr-image-analysis-nextflow/issues/7
    conda '/g/cba/miniconda3/envs/bia'

    script:
    """
    python ${segmentPythonScript} ${inputImagePath} "./" ${params.segmentSuffix} 
    """
}

process MEASURE {

}

workflow {
    images = Channel
        .fromPath(params.inputPattern)
        .map{ f ->
            sampleID = f.baseName
            return [sampleID, f]
         }

     labels = SEGMENT(images)

     MEASURE(labels.join(images))
}
