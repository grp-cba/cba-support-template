#!/bin/bash

# This script is shamelessly adapted from https://github.com/saalfeldlab/n5-utils, thanks @axtimwalde & co!

# This shell scripts uses maven to build an artifact of a java class that can be run using commandline

VERSION=0.0.1-SNAPSHOT # IMPORTANT: this should match the one in pom.xml file i.e. <version>0.0.1-SNAPSHOT</version>
MEM=8 # GB

mvn clean install -Denforcer.skip
mvn -Dmdep.outputFile=cp.txt -Dmdep.includeScope=runtime dependency:build-classpath

echo '#!/bin/bash' > print-string
echo '' >> print-string
echo "JAR=\$HOME/.m2/repository/de/embl/cba/print-string/${VERSION}/print-string-${VERSION}.jar" >> print-string
echo '/usr/lib/jvm/java-1.11.0-openjdk-amd64/bin/java \' >> print-string # change this path according to your local java dir
echo "  -Xmx${MEM}g \\" >> print-string
echo '  -XX:+UseConcMarkSweepGC \' >> print-string
echo -n '  -cp $JAR:' >> print-string
echo -n $(cat cp.txt) >> print-string
echo ' \' >> print-string
echo '  de.embl.cba.PrintStringCmd "$@"' >> print-string
chmod a+x print-string
echo "Successfully installed: cba-support-template/code/java/PrintStringCmd "
echo "Usage example:"
echo "./print-string -s HelloWorld"

rm cp.txt
