import argparse
from skimage.io import imread, imsave
from skimage.filters import threshold_otsu
from skimage.measure import label
from skimage import img_as_ubyte

def process_image(input_file_path, output_dir_path, output_suffix):
    # Open the image
    image = imread(input_file_path)
    
    # Apply Otsu thresholding
    thresh = threshold_otsu(image)
    binary_mask = image > thresh
    
    # Perform connected component analysis
    label_mask = label(binary_mask)
    
    # Save the labels mask
    file_name = os.path.basename(input_file_path)
    base_name = os.path.splitext(file_name)[0]
    output_path = os.path.join(output_dir_path, f'{base_name}_{output_suffix}.tif')   
    io.imsave(output_path, labels.astype(np.uint16), plugin='tifffile')

def main():
    # Set up argument parser
    #
    # For files please use "_file_path" as a suffix the variable name
    # For directories please use "_dir_path" as a suffix of the variable name
    #
    parser = argparse.ArgumentParser(description='Process an image with Otsu threshold and connected component labeling.')
    parser.add_argument('input_file_path', type=str, help='Path to the input image file.')
    parser.add_argument('-o', '--output_dir_path', type=str, help='Directory to save the output labels mask.')
    parser.add_argument('-s', '--output_suffix', type=str, help='Label mask suffix.')
    args = parser.parse_args()
    
    # Process the image
    process_image(args.input_file_path, args.output_dir_path, args.output_suffix)

if __name__ == '__main__':
    main()
