#### How to use this template project (DELETE this once done)

On your computer:
1. clone this repo on your computer.
1. copy the template to a new folder with the name of the actual project, e.g.  `cp -r cba-support-template NEW-PROJECT` (here and in all the lines of code below **replace** `NEW-PROJECT` with the proper name of your choice).
1. `cd NEW-PROJECT`
1. `git remote set-url origin git@git.embl.de:grp-cba/NEW-PROJECT.git`
1. `git push` (this will create the repo on Gitlab)

On Gitlab:
1. Add name/affiliation of collaborators to project description (Settings > General) and keywords, because the description is searchable.
1. Add topic keywords: `bioimage analysis`, `support`, `cba`, and other fitting ones
1. Create and add a project icon
1. In the end it should look similar to this example: https://git.embl.de/grp-cba/flatworm-analysis
1. Create project milestones as issues with due dates

For including screenshots, please put them in the "documentation" folder.
To make videos to be added to Gitlab, see [instructions](https://git.embl.de/grp-cba/cba/-/blob/master/misc/make-video-from-tif-fiji.md).

# Project title

This project is about ... *(add a handful of sentences)*

This project is exciting, because .... *(add a handful of sentences)*

## [Project metadata](project_metadata.md)

## [Data management](data_management.md)

## [Analysis workflow](analysis_workflow.md)

## Related work

- *Link to publication ...*
- *Link to repository ...*

## Publication guidelines

If your work approaches publication stage please get in touch with the bioimage analysis service team to discuss the below points.

- If the collaboration with the bioimage analysis service team involved continued consultancy sessions and **development of software that was used for the analysis of the published data**, please consider a **co-authorship**. If, instead, the contribution of the service team was less that that please consider an acknowledgement: "We thank XYZ of the Bioimage Analysis Support Team at the European Molecular Biology Laboratory (EMBL) for support."
- Please note that [EMBL's interal policy 71](https://www.embl.org/documents/wp-content/uploads/2021/12/Internal_Policy_IP71_Open_Science_and_Open_Access_Pub03122021.pdf) states: "EMBL expects all outputs of research and all aspects of service development funded by EMBL to be made openly available without delay. This refers to research articles, data, software, hardware designs, reagents, protocols and similar outputs."
- To make you **image data openly available** please upload it to the [BioImage Archive](https://www.ebi.ac.uk/bioimage-archive/).
    - BioImage Archive help:
        - [Submission portal](https://www.ebi.ac.uk/bioimage-archive/submit/)
        - [Online tutorial](https://www.ebi.ac.uk/training/online/courses/bioimage-archive-quick-tour/)
        - Contacts:
            - [Isabel Kemmer](https://www.embl.org/internal-information/people/isabel-kemmer/)
            - [bioimage-archive@ebi.ac.uk](mailto:bioimage-archive@ebi.ac.uk)
- To make the **analysis workflow reproducible**, please ensure that this GitLab repository (or a fork of it) is openly accessible, contains all code and also either contains or links to minimal example data for running the analysis workflow.
- In general, please follow the [guidelines for publishing bioimage data](https://www.nature.com/articles/s41592-023-01987-9.epdf).
- Ideally, please also make your analysis workflow accessible on [WorkflowHub](https://workflowhub.eu/) 




